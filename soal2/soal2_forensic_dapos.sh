#!/bin/bash

awk -F: '{
        if (NR == 2){
            start = $3;
        }
    }
    END{
        totalRequest = NR-1;
        totalHour = $3-start;
        average = totalRequest/totalHour;
        printf "Rata-rata serangan adalah sebanyak %.3f request per jam\n", average
    }
' ./log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt

awk -F'"' '
    {ip[$2] ++}
    END{
        ipMax = "";
        mxReq = 0;
        for (i in ip){
            if (mxReq < ip[i]){
                mxReq = ip[i];
                ipMax = i;
            }
        }
        printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", ipMax, mxReq
    }
' ./log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/result.txt

awk '
    /curl/ {++n}
    END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", n}
' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt

awk -F: '{
        if ($3 == 02 && !seen[$1]++){
            gsub(/"/, "");
            print $1
        }
    }
' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
