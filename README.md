# Soal Shift Modul 1 - D10
Penyelesaian Soal Shift Modul 1 Sistem Operasi 2021\
Kelompok D10
* Halyusa Ard Wahyudi - 5025201088
* Zahra Fayyadiyati - 5025201133
* Nathanael Roviery - 5025201258

---
## Table of Contents
* [Soal 1](#soal-1)
    * [Soal 1.a](#soal-1a)
    * [Soal 1.b](#soal-1b)
    * [Soal 1.c](#soal-1c)
    * [Soal 1.d](#soal-1d)
* [Soal 2](#soal-2)
    * [Soal 2.a](#soal-2a)
    * [Soal 2.b](#soal-2b)
    * [Soal 2.c](#soal-2c)
    * [Soal 2.d](#soal-2d)
    * [Soal 2.e](#soal-2e)
* [Soal 3](#soal-3)
    * [Soal 3.a](#soal-3a)
    * [Soal 3.b](#soal-3b)
    * [Soal 3.c](#soal-3c)
    * [Soal 3.d](#soal-3d)

# Soal 1
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1)

**Deskripsi:**\
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

## Soal 1.a
Source Code: [register.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/register.sh) [main.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/main.sh)

**Deskripsi:**\
Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

**Pembahasan:**\
Pendaftaran dilakukan dengan menggunakan bantuan awk dan >>.

``` bash
pass=0

if [ ! -e "users/user.txt" ]
then
        echo -n "" > users/user.txt
fi


echo "[REGISTER]"
read -ep "Enter new username: " username
read -s -ep "Enter new password: " password
echo ""

declare $( awk -F: -v var="$username" '{ 
                if($1 == var){ print "pass=1"; }
                else { print "pass=0" }}
	    END { print "temp=0"; }
        ' users/user.txt )
```

Pada awal register.sh, dilakukan pengecekan, apakan users/user.txt ada atau tidak. Apabila tidak ada, maka ia akan membuat file tersebut. Apabila register.sh dijalankan, maka dia akan menampilkan \[REGISTER] yang kemudian meminta user untuk memberikan username dan password untuk akun baru. Sebelum username didaftarkan pada users/user.txt, ia akan melakukan pengecekan dengan awk untuk menemukan apakah username yang diinputkan sudah terdaftar atau belum. Apabila sudah terdaftar, hasil dari awk tersebut akan mengubah variabel pass menjadi 1 dengan bantuan declare. END di akhir berfungsi untuk mencegah declare melakukan print ke layar user apabila yang diregistrasikan adalah akun pertama di sistem ini.

``` bash
userlog=0

echo "[LOGIN]"

read -ep "Enter your username: " username
read -s -ep "Enter your password: " password
echo ""

declare $( awk -F: -v usr="$username" -v psswrd="$password" '{ 
        if($1 == usr){ 
                print "userfound=1";
                if($2 == psswrd){ 
                        print "userlog=1";
                }
        }
        else { print "userfound=0"; }}
' users/user.txt )
```

Mirip seperti pada register.sh, main.sh juga akan menampilkan \[LOGIN] yang dilanjutkan dengan permintaan kepada user untuk memasukkan username dan password. Dengan menggunakan awk juga, apabila username ditemukan di users/user.txt, ia kemudian akan mengecek apakah password yang ada di line yang sama itu sama dengan password yang dimasukkan user. Dengan bantuan declare, ia akan mengeset userfound menjadi 1 apabila nama user ditemukan di users/user.txt dan mengeset userlog=1 apabila password yang dimasukkan adalah password yang benar.

## Soal 1.b
Source Code: [register.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/register.sh) [main.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/main.sh)

**Deskripsi:**\
Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

**Pembahasan:**\
Karena main.sh hanya melakukan penyamaan password, maka syarat-syarat tersebut cukup diimplementasikan di register.sh saja.

``` bash
if [ $pass -eq 1 ]
then
        echo "User already exists!"
        echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " REGISTER: ERROR User already exists" >> log.txt
else
        if [ $pass -ne 1 ]
        then
                # mengecek kelayakan password
                if [ ${#password} -lt 8 ]
                then
                        echo "Password must be at least 8 digits!"
                elif [[ ! $password =~ [A-Z] ]]
                then
                        echo "Password must contain at least 1 uppercase letter!"
                elif [[ ! $password =~ [a-z] ]]
                then
                        echo "Password must contain at least 1 lowercase letter!"
                elif [[ ! $password =~ [0-9] ]]
                then
                        echo "Password must contain at least 1 number!"
                elif [[ "$password" == "$username" ]]
                then
                        echo "Password must not same with username"
                else
                        pass=1
                        echo "Registration succesful!"
                        echo $username":"$password >> users/user.txt
                        echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
                        echo " REGISTER: INFO User $username registered successfully" >> log.txt
                fi
        fi
fi
```

Pada pembahasan sebelumnya, apabila pass=1, berarti user memasukkan username yang sudah terdaftar. Maka dari itu, apabila pass=1 sistem akan mengeluarkan “User already exists!”.

Apabila tidak, maka ia akan mengecek kelaya
kan password yang sudah diinputkan user. 
* Apabila password kurang dari 8 digit, sistem akan mengeluarkan "Password must be at least 8 digits!".
* Apabila password tidak mengandung uppercase, sistem akan mengeluarkan "Password must contain at least 1 uppercase letter!"
* Apabila password tidak mengandung lowercase, sistem akan mengeluarkan "Password must contain at least 1 lowercase letter!"
* Apabila password tidak mengandung angka, sistem akan mengeluarkan "Password must contain at least 1 number!"
* Apabila terdapat username di dalam passwordnya, sistem akan mengeluarkan "Password must not same with username"

Apabila password lolos dari semua persyaratan tersebut, maka username dan password akan dituliskan/didaftarkan ke dalam users/user.txt dan sistem akan mengeluarkan pesan "Registration succesful!"

## Soal 1.c
Source Code: [register.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/register.sh) [main.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/main.sh)

**Deskripsi:**\
Setiap percobaan login dan register akan tercatat pada log.txt dengan format :
MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user.
1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

**Pembahasan:**\
Penulisan log dilakukan setelah melakukan pengecekan, baik di main.sh dan register.sh

``` bash
if [ $pass -eq 1 ]
then
        echo "User already exists!"
        echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " REGISTER: ERROR User already exists" >> log.txt
```

Pada register.sh, apabila pass=1, ia akan mengeluarkan “User already exists!” dan kemudian memasukkan tanggal dan waktu ke dalam log.txt menggunakan bantuan date yang selanjutnya disusul dengan memasukkan “ REGISTER: ERROR User already exists” ke dalam log.txt tanpa newline.

``` bash
else
        pass=1
        echo "Registration succesful!"
        echo $username":"$password >> users/user.txt
        echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " REGISTER: INFO User $username registered successfully" >> log.txt
```

Apabila user berhasil mendaftarkan akun baru, setelah menampilkan pesan "Registration succesful!", sistem kemudian akan memasukkan tanggal, waktu dan pesan " REGISTER: INFO User $username registered successfully" ke dalam log.txt.

## Soal 1.d
Source Code: [register.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/register.sh) [main.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal1/main.sh)

**Deskripsi:**\
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai
berikut :
* dl N ( N = Jumlah gambar yang akan didownload) = Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
* att = Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

**Pembahasan:**\
Untuk membuat command dl dan att, digunakan read untuk memasukkan command ke variabel perintah dan operasi conditional pada variabel perintah.

``` bash
read -r perintah angka;
        if [[ "$perintah" == "dl" ]]
        then
                if [[ "$angka" == "" ]]
                then
                        echo "Please input any desired number after dl."
                else

                        tanggal=$(date +"%Y-%m-%d")
                        if [ ! -d "${tanggal}_${username}/" ]
                        then
                                mkdir ${tanggal}_${username}
                        else
                                unzip ${tanggal}_${username}
                        fi

                        while [ $counter -le $angka ]
                        do
                                if [ $counter -lt 10 ]
                                then
                                        inputNo="0${counter}"
                                else
                                        inputNo=${counter}
                                fi
                                wget https://loremflickr.com/320/240 -O ${tanggal}_${username}/PIC_${inputNo}
                                counter=$(($counter+1))
                        done

                        cd ${tanggal}_${username}
                        zip -re ../${tanggal}_${username}.zip * -P $password
                        cd ..
                        rm -r ${tanggal}_${username}


                fi
        elif [[ "$perintah" == "att" ]]
        then
                awk -v usr=$username -v n=0 '
                /LOGIN: INFO/ { 
                        if($6 == usr){ ++n;}}
                END { print "Number of successful attempt: ", n }' log.txt

                awk -v usr=$username -v n=0 '
                /LOGIN: ERROR/ { 
                        if($10 == usr){ ++n;}}
                END { print "Number of failed attempt: ", n }' log.txt
        fi
```

Apabila perintah adalah dl tetapi angka tidak dimasukkan oleh user, sistem akan menampilkan pesan “Please input any desired number after dl.". Apabila terdapat angka setelah dl, penulisan command benar sehingga selanjutnya akan disimpan tanggal hari ini ke variabel tanggal dan dibuat sebuah directory bernama ${tanggal}_${username} apabila tidak ada, dan akan melakukan unzip ${tanggal}_${username}.zip apabila ada zip dengan nama tersebut. Setelah itu, dilakukan while loop sebanyak angka yang mengikuti command dl . Di dalam loop ini, akan didownload gambar dari link yang tertera dan menyimpannya ke dalam direcotry ${tanggal}_${username} dengan nama PIC_${inputNo}, yang mana inputNo adalah counter dari loop tersebut. Apabila loop sudah selesai dan gambar sebanyak angka sudah didwonload, akan dilakukan penge-zip-an file terhadap directory ${tanggal}_${username} dengan cara mengubah direcotry supaya di dalam file zip, gambar tidak berada di dalam folder yang namanya sama dengan nama zip. Setelah itu, folder bernama ${tanggal}_${username} akan dihapus, sehingga hanya tersisa .zip-nya saja.

Apabila perintah adalah att, akan dijalankan awk untuk mencari dan menghitung berapa banyak LOGIN: INFO (yang menandakan bahwa terjadi keberhasilan login) dan LOGIN: ERROR (yang menandakan terjadi kegagalan login karena password salah) di file log.txt yang tentunya sesuai dengan username dari user yang sedang login sekarang.

# Soal 2
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal2)

**Deskripsi:**\
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur 
menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.
info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

## Soal 2.a
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal2/forensic_log_website_daffainfo_log)

**Deskripsi:**\
Membuat folder bernama 
[forensic_log_website_daffainfo_log](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal2/forensic_log_website_daffainfo_log)

## Soal 2.b
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

**Deskripsi:**\
Mencari rata-rata request per jam yang dikirimkan penyerang ke website lalu hasilnya dimasukkan ke sebuah file bernama 
[ratarata.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/ratarata.txt) di dalam folder
yang sudah dibuat sebelumnya

**Pembahasan:**\
Penyelesaian menggunakan AWK. Untuk mendapatkan kapan request pertama terjadi di tanggal 22 bisa menggunakan script sebagai berikut

``` bash
awk -F: '{
        if (NR == 2){
            start = $3;
        }
    }
```

* NR merupakan salah satu built-in variable yang memberikan baris ke berapa yang sedang di proses. Karena detail log dimulai saat baris ke-2, maka diberilah 
if-statement agar variable dapat diperoleh waktu request pertama (dalam satuan jam). 
* Tanda -F: merupakan Field Separator yang memisahkan argumen ketika terdapat karakter ':'.

``` bash
END{
        totalRequest = NR-1;
        totalHour = $3-start;
        average = totalRequest/totalHour;
        printf "Rata-rata serangan adalah sebanyak %.3f request per jam\n", average
    }
' ./log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/ratarata.txt
```

* Setelah itu, pada akhir AWK, diproses perhitungan rata-rata. Perhitungan dapat diperoleh sebagai berikut, 
  * **totalRequest** = NR-1 (karena baris pertama bukan log detail),
  * **totalHour** = (waktu akhir request)-(waktu awal request)
  * **average** = totalRequest/totalHour
  Lalu hasil rata-rata di print. Dalam hal ini, format angka rata-rata tiga angka dibelakang koma.
* Hasil output disimpan dalam folder [forensic_log_website_daffainfo_log](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal2/forensic_log_website_daffainfo_log) dalam format nama file [ratarata.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/ratarata.txt)

## Soal 2.c
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

**Deskripsi:**\
Menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut lalu hasilnya dimasukkan ke sebuah file bernama [result.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/result.txt) di dalam folder yang sudah dibuat sebelumnya

**Pembahasan:**\
Ide pencarian IP terbanyak ini menggunakan array dengan menghitung berapa banyak request setiap IP.

``` bash
awk -F'"' '
    {ip[$2] ++}
    END{
        ipMax = "";
        mxReq = 0;
        for (i in ip){
            if (mxReq < ip[i]){
                mxReq = ip[i];
                ipMax = i;
            }
        }
        printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", ipMax, mxReq
    }
' ./log_website_daffainfo.log > ./forensic_log_website_daffainfo_log/result.txt
```

* **$2** merupakan ip pengguna yang melakukan request ke server. Value dari array ip merupakan jumlah request yang dilakukan.
* **ipMax** merupakan IP pengguna yang melakukan request terbanyak.
* **mxReq** merupakan request terbanyak yang dilakukan oleh satu IP.
* Setelah diperoleh, di print sesuai dengan format yang diminta. Hasil output akan disimpan di [result.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/result.txt).

## Soal 2.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

**Deskripsi:**\
Mencari banyak request yang menggunakan user-agent **curl** dan masukkan outputnya ke dalam file [result.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/result.txt) di dalam folder yang sudah dibuat sebelumnya.

**Pembahasan:**\
Mencari log yang mengandung kata 'curl' pada tiap barisnya karena setiap 1 request merupakan 1 baris.

```bash
awk '
    /curl/ {++n}
    END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", n}
' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
```

* Apabila kata 'curl' ada pada baris, variable **n** ditambah 1.
* Di akhir baris, print output sesuai format dan dimasukan kedalam [result.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/result.txt).

## Soal 2.e
Source Code : [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

**Deskripsi:**\
Mencari daftar IP yang mengakses website pada jam 2 pagi. Kemudian masukkan daftar tersebut ke [result.txt](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/blob/main/soal2/forensic_log_website_daffainfo_log/result.txt) di dalam folder yang sudah dibuat sebelumnya.

**Pembahasan:**\
Melakukan print IP ketika waktu pada log menunjukkan 2 pagi.

``` bash
awk -F: '{
        if ($3 == 02 && !seen[$1]++){
            gsub(/"/, "");
            print $1
        }
    }
' ./log_website_daffainfo.log >> ./forensic_log_website_daffainfo_log/result.txt
```

* **-F:** merupakan Field Separator dengan karakter ":".
* **!seen[$1]++** merupakan array yang berisi value berapa kali melakukan request. Hal ini untuk mencegah terjadi duplikat print IP yang sama.
* **gsub(/"/, "")** untuk menghilangkan tanda kutip dua pada print nanti.

# Soal 3
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3)

**Deskripsi:**\
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untukmemperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

## Soal 3.a
Source Code: [minute_log.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3/minute_log.sh)

**Deskripsi:**\
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. \{YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

**Pembahasan:**\
Saat dijalankan, program shell akan memastikan kalau directory ~/log sudah ada dengan `-d "log"`. Apabila tidak ada, maka akan dibuat directory tersebut dengan mkdir.

``` bash
#!/bin/bash

exist=0
temp=0
shellPath=$(readlink -f minute_log.sh)

fileName=metrics_
currentTime=$(date "+%Y%m%d%H%M%S")
fileName=$fileName$currentTime

cd ~

if [ ! -d "log" ]
then
        mkdir log
fi

free -m > log/temp.txt
du -sh ~ >> log/temp.txt

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/$fileName.log

awk '{
	if(NR == 2 || NR == 3){
	       printf "%s,%s,%s,%s,%s,%s", $2, $3, $4, $5, $6, $7
	}
	else if(NR == 4){
		printf "%s,%s\n", $2, $1
	}	       
}' log/temp.txt >> log/$fileName.log
chmod ugo-rwx log/$fileName.log
chmod u+r log/$fileName.log
rm log/temp.txt
```

Selanjutnya, akan dimasukkan hasil dari `free -m` dan `du -sh ~` ke dalam temp.txt. Hal ini dilakukan untuk memudahkan penulisan ulang hasil dari kedua command tersebut sesuai dengan format yang ada pada note soal 3. Penulisan ulang ini menggunakan awk. Pertama, akan melakukan pengecekan dengan NR (Number of Record) yang merupakan baris ke-n dari iterasi awk. Sesuai pada format dari gabungan keluaran perintah free dan du, baris ke 2 hingga ke 4 adalah baris yang berisi data yang diperlukan. Selanjutnya, hasil dari awk akan dimasukkan ke dalam log/$fileName.log, menyusul keluaran echo sebelumnya yang juga dimasukkan ke log/$fileName.log.

## Soal 3.b
Source Code: [minute_log.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3/minute_log.sh)

**Deskripsi:**\
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

**Pembahasan:**\
Untuk dapat melakukan hal tersebut, digunakan perintah awk dan echo untuk menulis perintah dan jadwal ke dalam crontab.

``` bash
crontab -l > cronEdit

declare $( awk -v pth=$shellPath '
		{if($7 == pth){ print "exist=1" }}
                END { print "temp=0" }
        ' cronEdit)

if [ $exist -eq 0 ]
then
        echo "* * * * * bash $shellPath" >> cronEdit
        crontab cronEdit
fi

rm cronEdit
```

Pertama, hasil dari crontab -l dimasukkan ke dalam cronEdit, yang selanjutnya cronEdit akan digunakan untuk mencari dengan awk, apakah perintah sudah terdaftar di dalam crontab atau belum. Apabila terdaftar, maka akan di-set variabel exist menjadi 1. Pengubahan variabel temp=0 pada END adalah untuk menghindari terjadinya echo tidak penting ke dalam terminal. Apabila exist masih bernilai 0, berarti belum ada perintah di dalam crontab sehingga akan dilakukan echo ke dalam cronEdit, yang selanjutnya hasil dari cronEdit akan meng-overwrite/menggantikan semua baris yang ada di crontab. Setelah itu, cronEdit akan dihapus.

## Soal 3.c
Source Code: [aggregate_minutes_to_hourly_log.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3/aggregate_minutes_to_hourly_log.sh)

**Deskripsi:**\
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

**Pembahasan:**\
``` bash
#!/bin/bash



fileName=metrics_agg_
currentTime=$(date "+%Y%m%d%H")
currentHour=$(date "+%H")

fileName=$fileName$currentTime

counter=0

path=""
pathSize=""

minMemTotal=99999
minMemUsed=99999
minMemFree=99999
minMemShared=99999
minMemBuff=99999
minMemAvailable=99999
minSwapTotal=99999
minSwapUsed=99999
minSwapFree=99999

mxMemTotal=0
mxMemUsed=0
mxMemFree=0
mxMemShared=0
mxMemBuff=0
mxMemAvailable=0
mxSwapTotal=0
mxSwapUsed=0
mxSwapFree=0

avgMemTotal=0
avgMemUsed=0
avgMemFree=99999
avgMemShared=0
avgMemBuff=0
avgMemAvailable=0
avgSwapTotal=0
avgSwapUsed=0
avgSwapFree=0

```

* Pendeklarasian variable untuk mencari minimum, maximum, dan average

```bash
awk -F, -v minMemTotal=$minMemTotal_ -v minMemAvailable=$minMemAvailable_ -v minMemBuff=$minMemBuff_ -v minMemFree=$minMemFree_ -v minMemShared=$minMemShared_ -v minMemUsed=$minMemUsed_ -v minSwapFree=$minSwapFree_ -v minSwapTotal=$minSwapTotal_ -v minSwapUsed=minSwapUsed_ -v mxMemAvailable=$mxMemAvailable_ -v mxMemBuff=$mxMemBuff_ -v mxMemFree=$mxMemFree_ -v mxMemShared=mxMemShared_ -v mxMemTotal=mxMemTotal_ -v mxMemUsed=mxMemUsed_ -v mxSwapFree=$mxSwapFree_ -v mxSwapTotal=$mxSwapTotal_ -v mxSwapUsed=$mxSwapUsed -v avgMemTotal=$avgMemTotal_ -v avgMemAvailable=$avgMemAvailable_ -v avgMemBuff=$avgMemBuff_ -v avgMemFree=$avgMemFree_ -v avgMemShared=$avgMemShared_ -v avgMemUsed=$avgMemUsed_ -v avgSwapFree=$avgSwapFree_ -v avgSwapUsed=$avgSwapUsed_ -v avgSwapTotal=$avgSwapTotal_ -v counter=$counter_ -v path=$path_ -v pathSize=$pathSize '
    {counter++}
    {
        avgMemTotal = ((avgMemTotal + $1));
        avgMemUsed = ((avgMemUsed + $2));
        avgMemFree = ((avgMemFree + $3));
        avgMemShared = ((avgMemShared + $4));
        avgMemBuff = ((avgMemBuff + $5));
        avgMemAvailable = ((avgMemAvailable + $6));
        avgSwapTotal = ((avgSwapTotal + $7));
        avgSwapUsed = ((avgSwapUsed + $8))
        avgSwapFree = ((avgSwapFree + $9))

        if (minMemTotal -gt $1){
            minMemTotal = $1
        }
        if (minMemUsed -gt $2){
            minMemUsed = $2
        }
        if (minMemFree -gt $3){
            minMemFree = $3
        }
        if (minMemShared -gt $4){
            minMemShared = $4
        }
        if (minMemBuff -gt $5){
            minMemBuff = $5
        }
        if (minMemAvailable -gt $6){
            minMemUsed = $6
        }
        if (minSwapTotal -gt $7){
            minSwapTotal = $7
        }
        if (minSwapUsed -gt $8){
            minSwapUsed = $8
        }
        if (minSwapFree -gt $9){
            minSwapFree = $9
        }


        if (mxMemTotal -lt $1){
            mxMemTotal = $1
        }
        if (mxMemUsed -lt $2){
            mxMemUsed = $2
        }
        if (mxMemFree -lt $3){
            mxMemFree = $3
        }
        if (mxMemShared -lt $4){
            mxMemShared = $4
        }
        if (mxMemBuff -lt $5){
            mxMemBuff = $5
        }
        if (mxMemAvailable -lt $6){
            mxMemUsed = $6
        }
        if (mxSwapTotal -lt $7){
            mxSwapTotal = $7
        }
        if (mxSwapUsed -lt $8){
            mxSwapUsed = $8
        }
        if (mxSwapFree -lt $9){
            mxSwapFree = $9
        }

        path=$10
        pathSize=$11
    }

    END{
        counter = (counter / 2);
        avgMemTotal = (avgMemTotal / counter);
        avgMemUsed = (avgMemUsed / counter);
        avgMemFree = (avgMemFree / counter);
        avgMemUsed = (avgMemUsed / counter);
        avgMemBuff = (avgMemBuff / counter);
        avgMemAvailable = (avgMemAvailable / counter);
        avgSwapTotal = (avgSwapTotal / counter);
        avgSwapUsed = (avgSwapUsed / counter);
        avgSwapFree = (avgSwapFree / counter);

        printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"
        printf "minimum,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", minMemTotal, minMemUsed, minMemFree, minMemShared, minMemBuff, minMemAvailable, minSwapTotal, minSwapUsed, minSwapFree, path, pathSize
        printf "maximum,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", mxMemTotal, mxMemUsed, mxMemFree, mxMemShared, mxMemBuff, mxMemAvailable, mxSwapTotal, mxSwapUsed, mxSwapFree, path, pathSize
        printf "average,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", avgMemTotal, avgMemUsed, avgMemFree, avgMemShared, avgMemBuff, avgMemAvailable, avgSwapTotal, avgSwapUsed, avgSwapFree, path, pathSize
    }

' ~/log/metrics_????????$currentHour????.log > ~/log/$fileName.log

```

* Proses pencarian minimum dan maximum menggunakan if statement
* Counter berfungsi sebagai pembagi untuk nilai average di akhir pembacaan AWK

## Soal 3.d
Source Code: [minute_log.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3/minute_log.sh) [aggregate_minutes_to_hourly_log.sh](https://gitlab.com/roviery/soal-shift-sisop-modul-1-d10-2022/-/tree/main/soal3/aggregate_minutes_to_hourly_log.sh)

**Deskripsi:**\
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

**Pembahasan:**\

``` bash
chmod ugo-rwx log/$fileName.log
chmod u+r log/$fileName.log
```

Pada perintah chmod, u berarti user, g berarti group dan o berarti other. Sementara itu, r adalah read, w adalah write dan x adalah execute, dan + berarti memberikan permission dan - berarti menghapus permission. Maka dari itu, `chmod ugo-rwx` berarti menghapus semua permission (read, write dan execute) untuk user, group dan other. Sementara itu, chmod u+r berarti memberikan permission read untuk user/pemilik. Secara default, user adalah pemilik dan atau pembuat file.






