#!/bin/bash

pass=0

if [ ! -d "users/" ]
then
	mkdir users/
	echo -n "" > users/user.txt
fi


echo "[REGISTER]"
read -ep "Enter new username: " username
read -s -ep "Enter new password: " password
echo ""


declare $( awk -F: -v var="$username" '{ 
		if($1 == var){ print "pass=1"; }
		else { print "pass=0" }}
		END { print "temp=0"; }
	' users/user.txt )

if [ $pass -eq 1 ]
then
	echo "User already exists!"
	echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " REGISTER: ERROR User already exists" >> log.txt
else
	if [ $pass -ne 1 ]
	then
		# mengecek kelayakan password
		if [ ${#password} -lt 8 ]
		then
        		echo "Password must be at least 8 digits!"
		elif [[ ! $password =~ [A-Z] ]]
		then
        		echo "Password must contain at least 1 uppercase letter!"
		elif [[ ! $password =~ [a-z] ]]
		then
        		echo "Password must contain at least 1 lowercase letter!"
		elif [[ ! $password =~ [0-9] ]]
		then
        		echo "Password must contain at least 1 number!"
		elif [[ "$password" == "$username" ]]
		then
			echo "Password must not same with username"
		else
			pass=1
			echo "Registration succesful!"
			echo $username":"$password >> users/user.txt
			echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt 
			echo " REGISTER: INFO User $username registered successfully" >> log.txt
		fi
	fi
fi
