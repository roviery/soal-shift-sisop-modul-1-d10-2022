#!/bin/bash

userfound=0
userlog=0
counter=1

if [ ! -e "log.txt" ]
then
        echo -n "" > log.txt
fi


echo "[LOGIN]"

read -ep "Enter your username: " username
read -s -ep "Enter your password: " password
echo ""

declare $( awk -F: -v usr="$username" -v psswrd="$password" '{ 
	if($1 == usr){ 
		print "userfound=1";
		if($2 == psswrd){ 
			print "userlog=1";
		}
	}
	else { print "userfound=0"; }}
' users/user.txt )

if [ $userfound -eq 0 ]
then
	echo "Username not found!"
elif [ $userlog -eq 0 ]
then
	echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	echo "Wrong password!"
elif [ $userlog -eq 1 ]
then
	echo -n `date +"%m/%d/%y %H:%M:%S"` >> log.txt
        echo " LOGIN: INFO User $username logged in" >> log.txt
	echo "Log in successful."
	
	read -r perintah angka;
	if [[ "$perintah" == "dl" ]]
	then
		if [[ "$angka" == "" ]]
		then	
			echo "Please input any desired number after dl."
		else

			tanggal=$(date +"%Y-%m-%d")
			if [ ! -d "${tanggal}_${username}/" ]
			then
				mkdir ${tanggal}_${username}
			else
				unzip ${tanggal}_${username}
			fi

			while [ $counter -le $angka ]
			do	
				if [ $counter -lt 10 ]
				then
					inputNo="0${counter}"
				else
					inputNo=${counter}
				fi
				wget https://loremflickr.com/320/240 -O ${tanggal}_${username}/PIC_${inputNo}
				counter=$(($counter+1))
			done
			
			cd ${tanggal}_${username}
			zip -re ../${tanggal}_${username}.zip * -P $password
			cd ..
			rm -r ${tanggal}_${username}


		fi
	elif [[ "$perintah" == "att" ]]
        then
                awk -v usr=$username -v n=0 '
                /LOGIN: INFO/ { 
			if($6 == usr){ ++n;}}
                END { print "Number of successful attempt: ", n }' log.txt

                awk -v usr=$username -v n=0 '
                /LOGIN: ERROR/ { 
			if($10 == usr){ ++n;}}
                END { print "Number of failed attempt: ", n }' log.txt
        fi


fi 
