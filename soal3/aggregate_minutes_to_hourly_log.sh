#!/bin/bash



fileName=metrics_agg_
currentTime=$(date "+%Y%m%d%H")
currentHour=$(date "+%H")

fileName=$fileName$currentTime

counter=0

path=""
pathSize=""

minMemTotal=99999
minMemUsed=99999
minMemFree=99999
minMemShared=99999
minMemBuff=99999
minMemAvailable=99999
minSwapTotal=99999
minSwapUsed=99999
minSwapFree=99999

mxMemTotal=0
mxMemUsed=0
mxMemFree=0
mxMemShared=0
mxMemBuff=0
mxMemAvailable=0
mxSwapTotal=0
mxSwapUsed=0
mxSwapFree=0

avgMemTotal=0
avgMemUsed=0
avgMemFree=99999
avgMemShared=0
avgMemBuff=0
avgMemAvailable=0
avgSwapTotal=0
avgSwapUsed=0
avgSwapFree=0


awk -F, -v minMemTotal=$minMemTotal_ -v minMemAvailable=$minMemAvailable_ -v minMemBuff=$minMemBuff_ -v minMemFree=$minMemFree_ -v minMemShared=$minMemShared_ -v minMemUsed=$minMemUsed_ -v minSwapFree=$minSwapFree_ -v minSwapTotal=$minSwapTotal_ -v minSwapUsed=minSwapUsed_ -v mxMemAvailable=$mxMemAvailable_ -v mxMemBuff=$mxMemBuff_ -v mxMemFree=$mxMemFree_ -v mxMemShared=mxMemShared_ -v mxMemTotal=mxMemTotal_ -v mxMemUsed=mxMemUsed_ -v mxSwapFree=$mxSwapFree_ -v mxSwapTotal=$mxSwapTotal_ -v mxSwapUsed=$mxSwapUsed -v avgMemTotal=$avgMemTotal_ -v avgMemAvailable=$avgMemAvailable_ -v avgMemBuff=$avgMemBuff_ -v avgMemFree=$avgMemFree_ -v avgMemShared=$avgMemShared_ -v avgMemUsed=$avgMemUsed_ -v avgSwapFree=$avgSwapFree_ -v avgSwapUsed=$avgSwapUsed_ -v avgSwapTotal=$avgSwapTotal_ -v counter=$counter_ -v path=$path_ -v pathSize=$pathSize '
    {counter++}
    {
        avgMemTotal = ((avgMemTotal + $1));
        avgMemUsed = ((avgMemUsed + $2));
        avgMemFree = ((avgMemFree + $3));
        avgMemShared = ((avgMemShared + $4));
        avgMemBuff = ((avgMemBuff + $5));
        avgMemAvailable = ((avgMemAvailable + $6));
        avgSwapTotal = ((avgSwapTotal + $7));
        avgSwapUsed = ((avgSwapUsed + $8))
        avgSwapFree = ((avgSwapFree + $9))

        if (minMemTotal -gt $1){
            minMemTotal = $1
        }
        if (minMemUsed -gt $2){
            minMemUsed = $2
        }
        if (minMemFree -gt $3){
            minMemFree = $3
        }
        if (minMemShared -gt $4){
            minMemShared = $4
        }
        if (minMemBuff -gt $5){
            minMemBuff = $5
        }
        if (minMemAvailable -gt $6){
            minMemUsed = $6
        }
        if (minSwapTotal -gt $7){
            minSwapTotal = $7
        }
        if (minSwapUsed -gt $8){
            minSwapUsed = $8
        }
        if (minSwapFree -gt $9){
            minSwapFree = $9
        }


        if (mxMemTotal -lt $1){
            mxMemTotal = $1
        }
        if (mxMemUsed -lt $2){
            mxMemUsed = $2
        }
        if (mxMemFree -lt $3){
            mxMemFree = $3
        }
        if (mxMemShared -lt $4){
            mxMemShared = $4
        }
        if (mxMemBuff -lt $5){
            mxMemBuff = $5
        }
        if (mxMemAvailable -lt $6){
            mxMemUsed = $6
        }
        if (mxSwapTotal -lt $7){
            mxSwapTotal = $7
        }
        if (mxSwapUsed -lt $8){
            mxSwapUsed = $8
        }
        if (mxSwapFree -lt $9){
            mxSwapFree = $9
        }

        path=$10
        pathSize=$11
    }

    END{
        counter = (counter / 2);
        avgMemTotal = (avgMemTotal / counter);
        avgMemUsed = (avgMemUsed / counter);
        avgMemFree = (avgMemFree / counter);
        avgMemUsed = (avgMemUsed / counter);
        avgMemBuff = (avgMemBuff / counter);
        avgMemAvailable = (avgMemAvailable / counter);
        avgSwapTotal = (avgSwapTotal / counter);
        avgSwapUsed = (avgSwapUsed / counter);
        avgSwapFree = (avgSwapFree / counter);

        printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"
        printf "minimum,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", minMemTotal, minMemUsed, minMemFree, minMemShared, minMemBuff, minMemAvailable, minSwapTotal, minSwapUsed, minSwapFree, path, pathSize
        printf "maximum,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", mxMemTotal, mxMemUsed, mxMemFree, mxMemShared, mxMemBuff, mxMemAvailable, mxSwapTotal, mxSwapUsed, mxSwapFree, path, pathSize
        printf "average,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s\n", avgMemTotal, avgMemUsed, avgMemFree, avgMemShared, avgMemBuff, avgMemAvailable, avgSwapTotal, avgSwapUsed, avgSwapFree, path, pathSize
    }

' ~/log/metrics_????????$currentHour????.log > ~/log/$fileName.log

chmod ugo-rwx ~/log/$fileName.log
chmod u+r ~/log/$fileName.log