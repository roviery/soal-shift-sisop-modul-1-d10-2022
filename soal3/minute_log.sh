#!/bin/bash

exist=0
temp=0
shellPath=$(readlink -f minute_log.sh)

fileName=metrics_
currentTime=$(date "+%Y%m%d%H%M%S")
fileName=$fileName$currentTime

cd ~

if [ ! -d "log" ]
then
        mkdir log
fi

free -m > log/temp.txt
du -sh ~ >> log/temp.txt

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/$fileName.log

awk '{
	if(NR == 2 || NR == 3){
	       printf "%s,%s,%s,%s,%s,%s", $2, $3, $4, $5, $6, $7
	}
	else if(NR == 4){
		printf "%s,%s\n", $2, $1
	}	       
}' log/temp.txt >> log/$fileName.log
chmod ugo-rwx log/$fileName.log
chmod u+r log/$fileName.log
rm log/temp.txt

crontab -l > cronEdit

declare $( awk -v pth=$shellPath '
		{if($7 == pth){ print "exist=1" }}
                END { print "temp=0" }
        ' cronEdit)

if [ $exist -eq 0 ]
then
        echo "* * * * * bash $shellPath" >> cronEdit
        crontab cronEdit
fi

rm cronEdit
